class ServiceConfig < ActiveRecord::Base

  validates :name, presence: true
  validates :phone, presence: true
  validates :email, presence: true
  validate :atleast_one_is_checked

  def atleast_one_is_checked
    errors.add(:base, "Gotta have at least one service") unless website_needed || social_needed || video_needed || appdev_needed
  end
end

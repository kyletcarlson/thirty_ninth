json.array!(@service_configs) do |service_config|
  json.extract! service_config, :id, :name, :phone, :email, :website_needed, :social_needed, :video_needed, :appdev_needed, :desired_contract_length
  json.url service_config_url(service_config, format: :json)
end

json.extract! @service_config, :id, :name, :phone, :email, :website_needed, :social_needed, :video_needed, :appdev_needed, :desired_contract_length, :created_at, :updated_at

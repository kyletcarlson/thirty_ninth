Rails.application.routes.draw do
  resources :service_configs

  get 'welcome/index'

  root "welcome#index"
end

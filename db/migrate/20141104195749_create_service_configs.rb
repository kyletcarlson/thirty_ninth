class CreateServiceConfigs < ActiveRecord::Migration
  def change
    create_table :service_configs do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.boolean :website_needed, default: true
      t.boolean :social_needed
      t.boolean :video_needed
      t.boolean :appdev_needed
      t.integer :desired_contract_length

      t.timestamps
    end
  end
end
